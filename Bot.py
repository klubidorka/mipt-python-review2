from collections import defaultdict
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, BaseFilter
from pylab import plt
from scipy import polyfit, poly1d, linspace
from telegram import ReplyKeyboardMarkup
import sqlite3


class CustomFilter(BaseFilter):
    text = ''

    def __init__(self, filter_text):
        super().__init__()
        self.text = filter_text

    def filter(self, message):
        return message.text == self.text


# Основные функции: те, что имеют доступ к state и details
class BotBasis:
    # state принимает значения 'X values', 'Y values', 'x_err', 'y_err', 'x_name', 'y_name', 'title'
    # в зависимости от того, какие данные для построения графика нужно принять в данный момент.
    # Если значение состояния '', прием данных не ожидается
    state = defaultdict(dict)

    # details - словарик, в котором хранится информация о текущем графике
    details = defaultdict(dict)

    #  функции начинающиеся на 'get_' меняют состояние (state) на прием соответствующего значения
    def get_x(self, bot, update):
        self.getter(bot, update, 'X values', 'Insert X values space separated.', [])

    def get_y(self, bot, update):
        self.getter(bot, update, 'Y values', 'Insert Y values space separated.', [])

    def get_yerr(self, bot, update):
        self.getter(bot, update, 'y_err', 'Insert values for Y error bar space separated.', [])

    def get_xerr(self, bot, update):
        self.getter(bot, update, 'x_err', 'Insert values for X error bar space separated.', [])

    def get_xaxis(self, bot, update):
        self.getter(bot, update, 'x_name', 'Choose a name for X axis', '')

    def get_yaxis(self, bot, update):
        self.getter(bot, update, 'y_name', 'Choose a name for Y axis', '')

    def get_title(self, bot, update):
        self.getter(bot, update, 'title', 'Choose a title for your graph.', '')

    def getter(self, bot, update, data_type, text, reset_val):
        user_id = update.message.chat_id
        bot.send_message(chat_id=user_id, text=text)
        self.details[user_id][data_type] = reset_val
        self.state[user_id] = data_type

    # Проверка и запись данных в details
    def insert_data(self, bot, update):
        if self.state[update.message.chat_id] > '':
            user_id = update.message.chat_id
            nums = update.message.text.split()
            val_len = len(nums)
            if self.state[user_id] == 'title' or self.state[user_id] == 'x_name' \
                    or self.state[user_id] == 'y_name':
                self.details[user_id][self.state[user_id]] = update.message.text
                update.message.reply_text("got it")
                self.state[user_id] = ''
            elif not check_if_float(nums):
                update.message.reply_text("Incorrect values. Please, try again.")
            elif self.details[user_id]['val_num'] != 0\
                    and val_len != self.details[user_id]['val_num']:
                update.message.reply_text("Incorrect values. Please, try again.(1)")
            else:
                if self.details[user_id]['val_num'] == 0:
                    self.details[user_id]['val_num'] = val_len
                nums = list(map(float, nums))
                self.details[user_id][self.state[user_id]] = nums
                update.message.reply_text("got it")
                self.state[user_id] = ''

    # Сбросить данные построения графика
    def reset_data(self, bot, update):
        user_id = update.message.chat_id
        self.details[user_id]['title'] = ''
        self.details[user_id]['x_name'] = ''
        self.details[user_id]['y_name'] = ''
        self.details[user_id]['x_err'] = []
        self.details[user_id]['y_err'] = []
        self.details[user_id]['X values'] = []
        self.details[user_id]['Y values'] = []
        self.details[user_id]['val_num'] = 0

    # Вывод стартового меню
    def start(self, bot, update):
        self.reset_data(bot, update)
        custom_keyboard = [['/help'], ['Create a new graph']]
        reply_markup = ReplyKeyboardMarkup(custom_keyboard)
        bot.send_message(chat_id=update.message.chat_id,
                         text='Hello, {}! What do we do now?'.format(update.message.from_user.first_name),
                         reply_markup=reply_markup)

    # Функция построения графика
    def build_graph(self, bot, update):
        user_id = update.message.chat_id
        if self.details[user_id]['title'] == '':
            bot.send_message(chat_id=user_id,
                             text='Insert title first')
            return
        if len(self.details[user_id]['X values']) == 0:
            bot.send_message(chat_id=user_id,
                             text='Insert X values first')
            return
        if len(self.details[user_id]['Y values']) == 0:
            bot.send_message(chat_id=user_id,
                             text='Insert Y values first')
            return
        if self.details[user_id]['x_name'] == '':
            self.details[user_id]['x_name'] = 'X'
        if self.details[user_id]['y_name'] == '':
            self.details[user_id]['y_name'] = 'Y'
        bot.send_chat_action(chat_id=user_id, action='upload_photo')

        # Сохраняем данные графика
        conn = sqlite3.connect("mydb.db")
        cursor = conn.cursor()
        values = [update.message.from_user.id, design(self.details[user_id], user_id),
                  len(self.details[user_id]['X values']), self.details[user_id]['title']]
        bot.send_photo(chat_id=user_id, photo=open(str(user_id) + '.png', 'rb'))
        cursor.execute("INSERT INTO GraphInfo (user, equation,"
                       "dot_num, name) VALUES (?,?,?,?)", values)

        conn.commit()

        self.reset_data(bot, update)

# Вспомогательные функции


# Выводит список пользователей, строивших графики при помощи бота
def show_info(bot, update):
    if update.message.from_user.id != 408317277:
        bot.send_message(chat_id=update.message.chat_id,
                         text="You can't use this command.")
        return
    conn = sqlite3.connect("mydb.db")
    cursor = conn.cursor()
    users = []
    for row in cursor.execute("SELECT DISTINCT user from GraphInfo"):
        users.append(str(row[0]))
    report = "Users ID's:\n" + '\n'.join(users)
    bot.send_message(chat_id=update.message.chat_id,
                     text=report)


# Построение и подготовка к отправке графика
def design(details, user_id):
    plt.title(details['title'])
    plt.xlabel(details['x_name'])
    plt.ylabel(details['y_name'])
    plt.grid()

    # Наносим точки на график
    plt.scatter(details['X values'], details['Y values'])

    # Находим коэффициенты многочлена, задающего график наилучшей прямой
    fp = polyfit(details['X values'], details['Y values'], 1)
    f = poly1d(fp)

    # Добавляем легенду графика
    leg = [f]
    if list(f)[0] >= 0:
        location = "upper left"
    else:
        location = "upper right"
    plt.legend(leg, loc=location)

    # Наносим кресты погрешности на график
    if len(details['x_err']) > 0 and len(details['y_err']) > 0:
        plt.errorbar(details['X values'], details['Y values'], xerr=details['x_err'],
                     yerr=details['y_err'], ecolor='red', linestyle='None')

    # Проводим наилучшую прямую
    fx = linspace(min(details['X values']), max(details['X values']))
    plt.plot(fx, f(fx), linewidth=2)

    plt.savefig(str(user_id) + '.png', dpi=100)
    return str(f)


def bot_help(bot, update):
    bot.send_message(chat_id=update.message.chat_id,
                     text="I'm GraphBot and I"
                          " can build linear graphs for you. Tap "
                          "on 'Create a new graph' to make one")


# Вывод меню построения графика
def new_graph(bot, update):
    custom_keyboard = [['Title', 'Show graph', 'Reset values'],
                       ['X values', 'X-axis name', 'X error'],
                       ['Y values', 'Y-axis name', 'Y error']]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=update.message.chat_id,
                     text="Okay, insert values then. Fields 'Title', "
                          "'X values' and 'Y values' are compulsory.",
                     reply_markup=reply_markup)


def check_if_float(values):
    for num in values:
        try:
            float(num)
        except ValueError:
            return False
    return True


def create_database():
    conn = sqlite3.connect("mydb.db")
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS GraphInfo "
                   "(id INTEGER PRIMARY KEY, user INT, equation varchar(30),dot_num INT, name VARCHAR(30))")
    conn.commit()


def main():
    create_database()

    basis = BotBasis()

    updater = Updater(open('token.txt').read())
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", basis.start))
    dp.add_handler(CommandHandler("help", bot_help))
    dp.add_handler(CommandHandler("getinfo", show_info))

    dp.add_handler(MessageHandler(CustomFilter('Create a new graph'), new_graph))
    dp.add_handler(MessageHandler(CustomFilter('Show graph'), basis.build_graph))
    dp.add_handler(MessageHandler(CustomFilter('Title'), basis.get_title))
    dp.add_handler(MessageHandler(CustomFilter('X values'), basis.get_x))
    dp.add_handler(MessageHandler(CustomFilter('Y values'), basis.get_y))
    dp.add_handler(MessageHandler(CustomFilter('X-axis name'), basis.get_xaxis))
    dp.add_handler(MessageHandler(CustomFilter('Y-axis name'), basis.get_yaxis))
    dp.add_handler(MessageHandler(CustomFilter('X error'), basis.get_xerr))
    dp.add_handler(MessageHandler(CustomFilter('Y error'), basis.get_yerr))
    dp.add_handler(MessageHandler(CustomFilter('Reset values'), basis.reset_data))
    dp.add_handler(MessageHandler(Filters.text, basis.insert_data))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
